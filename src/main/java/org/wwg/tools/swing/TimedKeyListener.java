package org.wwg.tools.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.TreeSet;
import javax.swing.Timer;

/**
 * @see http://bugs.java.com/view_bug.do?bug_id=4153069
 */
public class TimedKeyListener implements KeyListener, ActionListener {
	private final TreeSet<Integer> set = new TreeSet<Integer>();
	private final Timer timer;

	private KeyEvent releaseEvent;
	private boolean gameModus = false;

	private void fireKeyReleased(KeyEvent e) {
		if (set.remove(new Integer(e.getKeyCode()))) {
			KeyReleased(e);
		}
	}

	public TimedKeyListener() {
		this(false);
	}

	public TimedKeyListener(boolean gameModus) {
		this.gameModus = gameModus;
		timer = new Timer(0, this);
	}

	public void KeyPressed(KeyEvent e) {}
	public void KeyReleased(KeyEvent e) {}
	public void KeyTyped(KeyEvent e) {}

	public int getPressedCount() {
		return set.size();
	}

	public void keyPressed(KeyEvent e) {
		if(timer.isRunning()) {
			timer.stop();
		} else {
			if (set.add(new Integer(e.getKeyCode()))) {
				if (gameModus) {
					KeyPressed(e);
					return;
				}
			}
		}
		if (!gameModus) {
			KeyPressed(e);
		}
	}

	public void keyReleased(KeyEvent e) {
		if(timer.isRunning()) {
			timer.stop();
			fireKeyReleased(e);
		} else {
			releaseEvent = e;
			timer.restart();
		}
	}

	public void keyTyped(KeyEvent e) {
		KeyTyped(e);
	}

	public void actionPerformed(ActionEvent e) {
		timer.stop();
		fireKeyReleased(releaseEvent);
	}
}
