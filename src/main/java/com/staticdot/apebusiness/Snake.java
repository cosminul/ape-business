package com.staticdot.apebusiness;

public class Snake extends Character {

	private static final int POSITION_CRAWL_LEFT_1 = 0;
	private static final int POSITION_CRAWL_LEFT_2 = 1;
	private static final int POSITION_CRAWL_RIGHT_1 = 2;
	private static final int POSITION_CRAWL_RIGHT_2 = 3;

	private final static int UPDATES_BETWEEN_STEPS = 10;

	private SpriteAnimator animator;
	private SpriteAnimator.SpriteSequence crawlLeftSequence;
	private SpriteAnimator.SpriteSequence crawlRightSequence;

	public Snake(Rect bounds) {
		super(Sprite.Transparency.TRANSLUCENT);
		this.bounds = bounds;
		gridCellDistance = 1;
		updatesBetweenSteps = UPDATES_BETWEEN_STEPS;
		updatesTillAnotherStep = updatesBetweenSteps;
		setupSprites();
		setupAnimation();
		startMovingRight();
	}

	private void setupSprites() {
		// Crawl left
		addResourceName(POSITION_CRAWL_LEFT_1, "snake01");
		addResourceName(POSITION_CRAWL_LEFT_2, "snake02");
		// Crawl right - the same sprites as for crawl left, but flipped horizontally
		addResourceName(POSITION_CRAWL_RIGHT_1, "snake01", true);
		addResourceName(POSITION_CRAWL_RIGHT_2, "snake02", true);
	}

	private void setupAnimation() {
		animator = new SpriteAnimator();
		// Crawl left
		crawlLeftSequence = new SpriteAnimator.SpriteSequence();
		crawlLeftSequence.addSpriteId(POSITION_CRAWL_LEFT_1);
		crawlLeftSequence.addSpriteId(POSITION_CRAWL_LEFT_2);
		animator.addSequence(crawlLeftSequence);
		// Crawl right
		crawlRightSequence = new SpriteAnimator.SpriteSequence();
		crawlRightSequence.addSpriteId(POSITION_CRAWL_RIGHT_1);
		crawlRightSequence.addSpriteId(POSITION_CRAWL_RIGHT_2);
		animator.addSequence(crawlRightSequence);
	}

	public void setNextFrame() {
		if(moveRight) {
			animator.setActiveSequence(crawlRightSequence);
		} else if(moveLeft) {
			animator.setActiveSequence(crawlLeftSequence);
		}
		activeSpriteId = animator.nextSpriteId();
	}

	@Override
	protected void updateGravity() {
		if(game.getScene().stairsHitByBox(bounds) != Rect.CollisionType.NO_COLLSION) {
			isGravityEnabled = false;
		} else {
			isGravityEnabled = true;			
		}
	}

	@Override
	protected void updateXPosition() {
		savedX = bounds.x;

		// Move left / right as requested
		if(moveLeft) {
			bounds.x -= gridCellDistance;
		} else if(moveRight) {
			bounds.x += gridCellDistance;
		}

		if(game.getScene().obstaclesHitByBox(bounds) != Rect.CollisionType.NO_COLLSION) {
			// This move would collide; reverse direction
			if(moveLeft) {
				stopMovingLeft();
				startMovingRight();
			} else if(moveRight) {
				stopMovingRight();
				startMovingLeft();
			}
		}
	}

	@Override
	protected void updateYPosition() {
		savedY = bounds.y;
	}
}
