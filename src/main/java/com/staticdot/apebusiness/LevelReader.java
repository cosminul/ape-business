package com.staticdot.apebusiness;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LevelReader {

	private Vector<LevelArea> obstacles;
	private Vector<LevelArea> stairs;
	private List<Banana> bananas;
	private List<Character> npcs;
	private Vector<Background> backgrounds;
	private Ape ape;

	public LevelReader() {
		obstacles = new Vector<LevelArea>();
		stairs = new Vector<LevelArea>();
		bananas = new LinkedList<Banana>();
		npcs = new LinkedList<Character>();
		backgrounds = new Vector<Background>();
		ape = new Ape();
	}

	private void readAreas(NodeList nodes, Vector<LevelArea> areaVector) {
		int totalObstacles = nodes.getLength();

		for(int i = 0; i < totalObstacles; i++) {
			Node obstacle = nodes.item(i);
			String name = obstacle.getAttributes().getNamedItem("name").getNodeValue();
			String xVal = obstacle.getAttributes().getNamedItem("x").getNodeValue();
			String yVal = obstacle.getAttributes().getNamedItem("y").getNodeValue();
			String wVal = obstacle.getAttributes().getNamedItem("w").getNodeValue();
			String hVal = obstacle.getAttributes().getNamedItem("h").getNodeValue();
			int x = Integer.parseInt(xVal);
			int y = Integer.parseInt(yVal);
			int w = Integer.parseInt(wVal);
			int h = Integer.parseInt(hVal);

			LevelArea area = new LevelArea(name, new Rect(x, y, w, h));

			areaVector.add(area);
		}
	}

	private void readBananas(NodeList nodes, List<Banana> bananaList) {
		int totalBananas = nodes.getLength();

		for(int i = 0; i < totalBananas; i++) {
			Node obstacle = nodes.item(i);
			String xVal = obstacle.getAttributes().getNamedItem("x").getNodeValue();
			String yVal = obstacle.getAttributes().getNamedItem("y").getNodeValue();
			String wVal = obstacle.getAttributes().getNamedItem("w").getNodeValue();
			String hVal = obstacle.getAttributes().getNamedItem("h").getNodeValue();
			int x = Integer.parseInt(xVal);
			int y = Integer.parseInt(yVal);
			int w = Integer.parseInt(wVal);
			int h = Integer.parseInt(hVal);

			Banana banana = new Banana(new Rect(x, y, w, h));

			bananaList.add(banana);
		}
	}

	private void readNPCs(NodeList nodes, List<Character> npcList) {
		int totalBananas = nodes.getLength();

		for(int i = 0; i < totalBananas; i++) {
			Node obstacle = nodes.item(i);
			String type = obstacle.getAttributes().getNamedItem("type").getNodeValue();
			String xVal = obstacle.getAttributes().getNamedItem("x").getNodeValue();
			String yVal = obstacle.getAttributes().getNamedItem("y").getNodeValue();
			String wVal = obstacle.getAttributes().getNamedItem("w").getNodeValue();
			String hVal = obstacle.getAttributes().getNamedItem("h").getNodeValue();
			int x = Integer.parseInt(xVal);
			int y = Integer.parseInt(yVal);
			int w = Integer.parseInt(wVal);
			int h = Integer.parseInt(hVal);

			if(type.equalsIgnoreCase("snake")) {
				Character npc = new Snake(new Rect(x, y, w, h));
				npcList.add(npc);
			}
		}
	}

	private void readBackgrounds(NodeList nodes, Vector<Background> bgVector) {
		int totalBackgrounds = nodes.getLength();

		for(int i = 0; i < totalBackgrounds; i++) {
			Node obstacle = nodes.item(i);
			String name = obstacle.getAttributes().getNamedItem("name").getNodeValue();
			String xVal = obstacle.getAttributes().getNamedItem("x").getNodeValue();
			String yVal = obstacle.getAttributes().getNamedItem("y").getNodeValue();
			String wVal = obstacle.getAttributes().getNamedItem("w").getNodeValue();
			String hVal = obstacle.getAttributes().getNamedItem("h").getNodeValue();
			int x = Integer.parseInt(xVal);
			int y = Integer.parseInt(yVal);
			int w = Integer.parseInt(wVal);
			int h = Integer.parseInt(hVal);

			Background bg = new Background();
			bg.setBounds(x, y, w, h);
			bg.setResourceName(name);
			bgVector.add(bg);
		}
	}

	private void readApe(Node node, Ape ape) {
		String xVal = node.getAttributes().getNamedItem("x").getNodeValue();
		String yVal = node.getAttributes().getNamedItem("y").getNodeValue();
		int x = Integer.parseInt(xVal);
		int y = Integer.parseInt(yVal);

		ape.setPosition(x, y);
	}

	public void readLevel() {
		String xmlFile = getClass().getResource("/levels/level01.xml").toExternalForm();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList listOfObstacles = doc.getElementsByTagName("obstacle");
			readAreas(listOfObstacles, obstacles);
			NodeList listOfStairs = doc.getElementsByTagName("stair");
			readAreas(listOfStairs, stairs);
			NodeList listOfBananas = doc.getElementsByTagName("banana");
			readBananas(listOfBananas, bananas);
			NodeList listOfNPCs = doc.getElementsByTagName("npc");
			readNPCs(listOfNPCs, npcs);
			NodeList listOfBackgrounds = doc.getElementsByTagName("background");
			readBackgrounds(listOfBackgrounds, backgrounds);
			NodeList listOfApes = doc.getElementsByTagName("ape");
			readApe(listOfApes.item(0), ape);

		} catch(ParserConfigurationException pce) {
			System.err.println(pce);
		} catch(IOException ioe) {
			System.err.println(ioe);
		} catch(SAXException saxe) {
			System.err.println(saxe);
		}
	}

	public Vector<LevelArea> getObstacles() {
		return obstacles;
	}

	public Vector<LevelArea> getStairs() {
		return stairs;
	}

	public List<Banana> getBananas() {
		return bananas;
	}

	public List<Character> getNPCs() {
		return npcs;
	}

	public Vector<Background> getBackgrounds() {
		return backgrounds;
	}

	public Ape getApe() {
		return ape;
	}
}
