package com.staticdot.apebusiness;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Sprite {
	private BufferedImage img;
	private Transparency transparency;
	private Graphics graphics;
	private boolean flipped;

	public enum Transparency {
		OPAQUE,
		TRANSLUCENT
	}

	/**
	 * Creates a new sprite
	 * @param width the width of the new sprite, in pixels
	 * @param height the height of the new sprite, in pixels
	 * @param transparency the sprite's transparency
	 */
	public Sprite(int width, int height, Transparency transparency) {
		this.transparency = transparency;
		flipped = false;
		createSprite(width, height);
	}

	public Graphics getGraphics() {
		if(graphics == null) {
			graphics = img.getGraphics();
		}
		return graphics;
	}

	private void createSprite(int width, int height) {
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = env.getDefaultScreenDevice();
		GraphicsConfiguration config = device.getDefaultConfiguration();
		int awtTransparency = java.awt.Transparency.OPAQUE;
		switch(transparency) {
		case OPAQUE:
			awtTransparency = java.awt.Transparency.OPAQUE;
			break;
		case TRANSLUCENT:
			awtTransparency = java.awt.Transparency.TRANSLUCENT;
			break;
		default:
			throw new RuntimeException("Invalid transparency");
		}
		img = config.createCompatibleImage(width, height, awtTransparency);
	}

	public void setFlipped(boolean flipped) {
		this.flipped = flipped;
	}

	public boolean isFlipped() {
		return flipped;
	}

	public void setResolution(int width, int height) {
		createSprite(width, height);
	}

	public void loadResource(String resName) throws IOException {
		BufferedImage raw = ImageIO.read(getClass().getResource(resName));
		createSprite(raw.getWidth(null), raw.getHeight(null));

		if(flipped) {
			// Flip the image horizontally
			AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
			tx.translate(-raw.getWidth(null), 0);
			AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			raw = op.filter(raw, null);
		}

		Graphics g = img.getGraphics();
		g.drawImage(raw, 0, 0, null);
	}

	public void blit(Sprite canvas, int x, int y) {
		drawImage(canvas.getGraphics(), x, y);
	}

	public void blit(Graphics g, int x, int  y) {
		drawImage(g, x, y);
	}

	protected void drawImage(Graphics g, int x, int y) {
		g.drawImage(img, x, y, null);
	}

	public void drawLine(int x1, int y1, int x2, int y2) {
		getGraphics().drawLine(x1, y1, x2, y2);
	}

	public void setColor(int r, int g, int b) {
		getGraphics().setColor(new Color(r, g, b));
	}

	public void drawRect(int x, int y, int width, int height) {
		getGraphics().drawRect(x, y, width, height);
	}

	public void fillRect(int x, int y, int width, int height) {
		getGraphics().fillRect(x, y, width, height);
	}
}
