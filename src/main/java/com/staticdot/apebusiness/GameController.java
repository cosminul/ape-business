package com.staticdot.apebusiness;

import java.util.Iterator;

public class GameController {

	private Ape ape;
	private Scene scene;
	private AudioPlayer musicPlayer;
	private AudioPlayer soundPlayer;

	private boolean gameOver = false; // for game termination
	private boolean isPaused = false;

	public GameController(int screenWidth, int screenHeight) {
		scene = new Scene(screenWidth, screenHeight);
		for(Character npc: scene.getNPCs()) {
			npc.setGameController(this);
		}
		ape = scene.getApe();
		ape.setGameController(this);
		musicPlayer = new SampledAudioPlayer();
		musicPlayer.play(AudioPlayer.MUSIC_SOUND_1, true);
		soundPlayer = new SampledAudioPlayer();
	}

	public Scene getScene() {
		return scene;
	}

	public void startLeft() {
		ape.startMovingLeft();
	}

	public void stopLeft() {
		ape.stopMovingLeft();
	}

	public void startRight() {
		ape.startMovingRight();
	}

	public void stopRight() {
		ape.stopMovingRight();
	}

	public void startUp() {
		ape.startMovingUp();
	}

	public void stopUp() {
		ape.stopMovingUp();
	}

	public void startDown() {
		ape.startMovingDown();
	}

	public void stopDown() {
		ape.stopMovingDown();
	}

	public void toggleMuteMusic() {
		musicPlayer.toggleMute();
	}

	public void toggleMuteSound() {
		soundPlayer.toggleMute();
	}

	public void startTouch(int x, int y) {}
	public void endTouch(int x, int y) {}

	public void touch(int x, int y) {
		if(!isPaused && !gameOver) {
			// Do  something
		}
	}

	public void pauseGame() {
		isPaused = true;
	}

	public void resumeGame() {
		isPaused = false;
	}

	public void gameUpdate() {
		if(!isPaused && !gameOver) {
			// Update game state
			ape.updatePosition();
			for(Iterator<Banana> it = scene.getBananas().iterator(); it.hasNext(); ) {
				Banana banana = it.next();
				if(ape.getBounds().collides(banana.getBounds())) {
					it.remove();
					scene.removeElement(banana);
					soundPlayer.play(AudioPlayer.EAT_SOUND);
				} else {
					banana.update();
				}
			}
			for(Iterator<Character> it = scene.getNPCs().iterator(); it.hasNext(); ) {
				Character npc = it.next();
				npc.updatePosition();
				if(ape.getBounds().collides(npc.getBounds())) {
					ape.die();
				}
			}
		}
	}

	public void gameRender() {
		scene.render();
		if(gameOver) {
			// print a game over message
		}
	}

	public void setResolution(int width, int height) {
		scene.setScreenResolution(width, height);
	}

	public void toggleDebugMode() {
		scene.toggleDebugMode();
	}
}
