package com.staticdot.apebusiness;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class Scene {

	private class ScreenResolution {
		public int width;
		public int height;

		public ScreenResolution(int width, int height) {
			this.width = width;
			this.height = height;
		}
	}

	private Vector<DrawableSceneElement> drawableElements;
	private Sprite screen;
	private Vector<ScreenResolution> possibleResolutions;

	private Vector<LevelArea> obstacles;
	private Vector<LevelArea> stairs;
	private List<Banana> bananas;
	/** The list of NPCs (non-player characters) */
	private List<Character> npcs;
	private Vector<Background> backgrounds;
	private Ape ape;

	public final static int GRID_WIDTH = 160;
	public final static int GRID_HEIGHT = 120;

	private int screenWidth;
	private int screenHeight;
	private int gridPixelStep;

	private boolean debugMode;

	public Scene(int screenWidth, int screenHeight) {
		possibleResolutions = new Vector<ScreenResolution>();
		possibleResolutions.add(new ScreenResolution(800, 600));
		possibleResolutions.add(new ScreenResolution(640, 480));
		drawableElements = new Vector<DrawableSceneElement>();
		setScreenResolution(screenWidth, screenHeight);

		LevelReader reader = new LevelReader();
		reader.readLevel();
		obstacles = reader.getObstacles();
		stairs = reader.getStairs();
		bananas = reader.getBananas();
		npcs = reader.getNPCs();
		backgrounds = reader.getBackgrounds();
		ape = reader.getApe();

		for(Background bg: backgrounds) {
			addElement(bg);
		}
		for(Banana banana: bananas) {
			addElement(banana);
		}
		for(Character npc: npcs) {
			addElement(npc);
		}
		addElement(ape);
	}

	public Sprite getScreen() {
		return screen;
	}

	public void setScreenResolution(int width, int height) {
		boolean matched = false;
		for(ScreenResolution resolution: possibleResolutions) {
			if(resolution.height <= height && resolution.width <= width) {
				screenWidth = resolution.width;
				screenHeight = resolution.height;
				matched = true;
				break;
			}
		}
		if(!matched) {
			System.err.println("Your screen resolution is not supported: " + width + "x" + height);
		}
		// TODO make sure the remainder is 0
		// Also make sure height/GRID_HEIGHT is the same value
		gridPixelStep = screenWidth / GRID_WIDTH;
		// (Re-)Create the buffer
		screen = new Sprite(screenWidth, screenHeight, Sprite.Transparency.OPAQUE);
		if(screen == null) {
			System.err.println("Error creating screen buffer");
		}
		// Set the resolution of the game elements
		try {
			for(DrawableSceneElement element: drawableElements) {
				element.setResolution(screenWidth, screenHeight);
			}
		} catch(IOException ioe) {
			System.err.println("Could not load resources");
		}
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public void addElement(DrawableSceneElement element) {
		drawableElements.add(element);
		try {
			element.setResolution(screenWidth, screenHeight);
		} catch (IOException e) {
			System.err.println("Could not load resources");
		}
	}

	public void removeElement(DrawableSceneElement element) {
		drawableElements.remove(element);
	}

	public List<Banana> getBananas() {
		return bananas;
	}

	public List<Character> getNPCs() {
		return npcs;
	}

	public Ape getApe() {
		return ape;
	}

	public Rect.CollisionType obstaclesTouchedByBox(Rect box) {
		Iterator<LevelArea> it = obstacles.iterator();
		while(it.hasNext()) {
			Rect obstacle = it.next().getArea();
			if(box.collides(obstacle) || box.touches(obstacle)) {
				return box.getCollisionType(obstacle);
			}
		}
		return Rect.CollisionType.NO_COLLSION;
	}

	public Rect.CollisionType obstaclesHitByBox(Rect box) {
		Iterator<LevelArea> it = obstacles.iterator();
		while(it.hasNext()) {
			Rect obstacle = it.next().getArea();
			if(box.collides(obstacle)) {
				return box.getCollisionType(obstacle);
			}
		}
		return Rect.CollisionType.NO_COLLSION;
	}

	public Rect.CollisionType stairsHitByBox(Rect box) {
		Iterator<LevelArea> it = stairs.iterator();
		while(it.hasNext()) {
			Rect stair = it.next().getArea();
			if(box.collides(stair)) {
				return box.getCollisionType(stair);
			}
		}
		return Rect.CollisionType.NO_COLLSION;
	}

	public void render() {
		// Draw game elements
		for(DrawableSceneElement element: drawableElements) {
			if(element.isVisible()) {
				blit(element);
			}
		}
		if(debugMode) {
			renderForDebug();
		}
	}

	public void toggleDebugMode() {
		debugMode = !debugMode;
	}

	private void blit(DrawableSceneElement element) {
		// Convert world coordinates to screen coordinates
		int x = element.getBounds().x * gridPixelStep;
		int y = element.getBounds().y * gridPixelStep;
		element.getActiveSprite().blit(screen, x, y);
	}

	private void renderForDebug() {
		showGrid();
		showObstacles();
		showStairs();
		// Show the bounds of the drawable elements
		screen.setColor(255, 0, 255);
		for(DrawableSceneElement element: drawableElements) {
			// Convert world coordinates to screen coordinates
			int x = element.getBounds().x * gridPixelStep;
			int y = element.getBounds().y * gridPixelStep;
			int w = element.getBounds().w * gridPixelStep;
			int h = element.getBounds().h * gridPixelStep;
			screen.drawRect(x, y, w - 1, h - 1);
		}
	}

	private void showGrid() {
		screen.setColor(0, 128, 0);
		int count = 0;
		for(int x = 0; x < GRID_WIDTH; x++) {
			screen.drawLine(x * gridPixelStep, 0, x * gridPixelStep, GRID_HEIGHT * gridPixelStep);
			if(count % 10 == 0) {
				screen.fillRect(x * gridPixelStep, 0, gridPixelStep, gridPixelStep);
			}
			count++;
		}
		count = 0;
		for(int y = 0; y < GRID_HEIGHT; y++) {
			screen.drawLine(0, y * gridPixelStep, GRID_WIDTH * gridPixelStep, y * gridPixelStep);
			if(count % 10 == 0) {
				screen.fillRect(0, y * gridPixelStep, gridPixelStep, gridPixelStep);
			}
			count++;
		}
	}

	private void showObstacles() {
		screen.setColor(255, 0, 255);
		Iterator<LevelArea> it = obstacles.iterator();
		while(it.hasNext()) {
			Rect r = it.next().getArea();
			// Convert world coordinates to screen coordinates
			int x = r.x * gridPixelStep;
			int y = r.y * gridPixelStep;
			int w = r.w * gridPixelStep;
			int h = r.h * gridPixelStep;
			screen.drawRect(x, y, w, h);
		}
	}

	private void showStairs() {
		screen.setColor(255, 128, 0);
		Iterator<LevelArea> it = stairs.iterator();
		while(it.hasNext()) {
			Rect r = it.next().getArea();
			// Convert world coordinates to screen coordinates
			int x = r.x * gridPixelStep;
			int y = r.y * gridPixelStep;
			int w = r.w * gridPixelStep;
			int h = r.h * gridPixelStep;
			screen.drawRect(x, y, w, h);
		}
	}
}
