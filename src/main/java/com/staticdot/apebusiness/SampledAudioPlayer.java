package com.staticdot.apebusiness;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SampledAudioPlayer implements AudioPlayer, Runnable { 

	private String resFile;
	private boolean looping;
	private boolean mute;

	private final int EXTERNAL_BUFFER_SIZE = 32 * 1024; 

	public SampledAudioPlayer() {
		looping = false;
		mute = false;
	}

	public void play(String soundResource) {
		play(soundResource, false);
	}

	public void play(String soundResource, boolean loop) {
		resFile = "/sounds/" + soundResource + ".wav";
		looping = loop;

		if(!mute) {
			new Thread(this).start();
		}
	}

	public void stop() {
		mute = true;
	}

	public void toggleMute() {
		mute = !mute;
		if(!mute && looping) {
			new Thread(this).start();
		}
	}

	public void run() {
		AudioInputStream audioInputStream = null;
		try {
			URL audioSource = tempFile(resFile).toURL();
			audioInputStream = AudioSystem.getAudioInputStream(audioSource);
		} catch (UnsupportedAudioFileException uafe) { 
			uafe.printStackTrace();
			return;
		} catch (IOException ioe) { 
			ioe.printStackTrace();
			return;
		} 

		AudioFormat format = audioInputStream.getFormat();
		SourceDataLine line = null;
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);

		try { 
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(format);
		} catch (LineUnavailableException e) { 
			e.printStackTrace();
			return;
		} catch (Exception e) { 
			e.printStackTrace();
			return;
		} 

		line.start();
		int bytesRead = 0;
		byte[] data = new byte[EXTERNAL_BUFFER_SIZE];

		try {
			audioInputStream.mark(Integer.MAX_VALUE);
			while(!mute) {
				bytesRead = audioInputStream.read(data, 0, data.length);
				if(bytesRead >= 0) {
					line.write(data, 0, bytesRead);
				}
				// If the end of the stream was reached...
				if(bytesRead == -1) {
					if(looping) {
						// rewind
						audioInputStream.reset();
					} else {
						// quit the loop
						break;
					}
				}
				Thread.yield();
			}
		} catch (IOException e) { 
			e.printStackTrace();
		} finally {
			line.drain();
			line.close();
		}
	}

	private URI tempFile(String srcName) {
		File tmp = null;

		try {
			tmp = File.createTempFile("ape", null);
			tmp.deleteOnExit();

			InputStream src = getClass().getResourceAsStream(srcName);
			OutputStream dst = new FileOutputStream(tmp);

			// Transfer bytes from src to dst
			byte[] buf = new byte[1024];
			int len;
			while((len = src.read(buf)) > 0) {
				dst.write(buf, 0, len);
			}
			src.close();
			dst.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		if(tmp == null) {
			return null;
		} else {
			return tmp.toURI();
		}
	}
}
