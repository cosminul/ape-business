package com.staticdot.apebusiness;

import java.util.Vector;

/**
 * A collection of SpriteSequence objects.
 * @author Cosmin
 */
public class SpriteAnimator {

	/**
	 * A circular sequence of sprite IDs.
	 * @author Cosmin
	 */
	public static class SpriteSequence {
		private Vector<Integer> sprites;
		private int cursor;

		/**
		 * Creates a new empty sprite ID sequence.
		 */
		public SpriteSequence() {
			sprites = new Vector<Integer>();
		}

		/**
		 * Removes all the sprite IDs from the sequence.
		 */
		public void clear() {
			sprites.clear();
		}

		/**
		 * Adds a new sprite ID to the sequence.
		 * @param spriteId the sprite ID to be added
		 */
		public void addSpriteId(int spriteId) {
			sprites.add(spriteId);
		}

		/**
		 * Move to the beginning of the sequence.
		 */
		public void reset() {
			cursor = 0;
		}

		/**
		 * Returns the next sprite ID in the sequence. If the sequence
		 * reached its end, restart from the beginning.
		 * @return the next sprite ID in the sequence
		 */
		public int nextSpriteId() {
			cursor++;
			if(cursor >= sprites.size()) {
				cursor = 0;
			} 
			return sprites.elementAt(cursor);
		}
	}

	private Vector<SpriteSequence> sequences;
	private SpriteSequence activeSequence; 

	/**
	 * Creates a new SpriteAnimator
	 */
	public SpriteAnimator() {
		sequences = new Vector<SpriteSequence>();
	}

	/**
	 * Removes all the sequences from the animator.
	 */
	public void clear() {
		sequences.clear();
	}

	/**
	 * Adds a new sequence to the animator, and make it active if
	 * no sequence was already active.
	 * @param sequence the sequence to be added
	 */
	public void addSequence(SpriteSequence sequence) {
		sequences.add(sequence);
		if(activeSequence == null) {
			activeSequence = sequence;
		}
	}

	/**
	 * Switches the active sequence to the specified sequence.
	 * @param sequence the sequence to be set as active
	 */
	public void setActiveSequence(SpriteSequence sequence) {
		if(activeSequence == sequence) {
			return;
		}
		sequence.reset();
		activeSequence = sequence;
	}

	/**
	 * Returns the next sprite ID in the active sequence.
	 * @return the next sprite ID in the active sequence
	 */
	public int nextSpriteId() {
		return activeSequence.nextSpriteId();
	}
}
