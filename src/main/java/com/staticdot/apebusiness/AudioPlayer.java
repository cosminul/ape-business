package com.staticdot.apebusiness;

public interface AudioPlayer {

	// self-recorded
	public final static String EAT_SOUND = "eat";
	// http://www.free-samples-n-loops.com/free-guitar-loops.html
	public final static String MUSIC_SOUND_1 = "music_1";

	public void play(String soundResource);
	public void play(String soundResource, boolean loop);
	public void stop();
	public void toggleMute();
}
