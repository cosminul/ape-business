package com.staticdot.apebusiness;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class DrawableSceneElement {

	private class Resource {
		private String resourceName;
		private Sprite sprite;
		private boolean flipped;
	}

	private class Position {
		public int x;
		public int y;

		public Position(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private Map<Integer, Resource> resources;
	protected Integer activeSpriteId;
	/** World coordinates */
	protected Rect bounds;
	protected Position defaultPos;
	/** Screen dimensions */
	private int width;
	private int height;
	private Sprite.Transparency transparency;
	private boolean visible;

	public DrawableSceneElement(Sprite.Transparency transparency) {
		resources = new HashMap<Integer, Resource>();
		this.transparency = transparency;
		visible = true;
	}

	public void addResourceName(int id, String resourceName) {
		addResourceName(id, resourceName, false);
	}

	public void addResourceName(int id, String resourceName, boolean flipped) {
		// The first one is the default active
		if(resources.size() == 0) {
			activeSpriteId = id;
		}
		Resource resource = new Resource();
		resource.resourceName = resourceName;
		resource.flipped = flipped;
		resources.put(new Integer(id), resource);
	}

	public void setResolution(int width, int height) throws IOException {
		this.width = width;
		this.height = height;
		for(Integer resId: resources.keySet()) {
			Resource resource = resources.get(resId);
			Sprite sprite = new Sprite(bounds.w, bounds.h, transparency);
			sprite.setResolution(width, height);
			sprite.setFlipped(resource.flipped);
			loadResource(sprite, resource.resourceName);
			resource.sprite = sprite;
		}
	}

	private void loadResource(Sprite sprite, String basename) throws IOException {
		sprite.loadResource("/images/" + basename + "_" + width + "x" + height + ".png");
	}

	public Rect getBounds() {
		return bounds;
	}

	public void setBounds(int x, int y, int width, int height) {
		bounds.x = x;
		bounds.y = y;
		bounds.w = width;
		bounds.h = height;
	}

	public void setPosition(int x, int y) {
		bounds.x = x;
		bounds.y = y;
		if(defaultPos == null) {
			defaultPos = new Position(x, y);
		}
	}

	public void restoreDefaultPosition() {
		bounds.x = defaultPos.x;
		bounds.y = defaultPos.y;
	}

	public Sprite getActiveSprite() {
		return resources.get(activeSpriteId).sprite;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isVisible() {
		return visible;
	}

	public void toggleVisibility() {
		visible = !visible;
	}

	protected abstract void setNextFrame();
}
