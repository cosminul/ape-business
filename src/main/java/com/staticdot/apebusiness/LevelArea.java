package com.staticdot.apebusiness;

public class LevelArea {

	private String name;
	private Rect area;

	public LevelArea(String name, Rect area) {
		this.name = name;
		this.area = area;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rect getArea() {
		return area;
	}

	public void setArea(Rect area) {
		this.area = area;
	}
}
