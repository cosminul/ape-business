package com.staticdot.apebusiness;

public abstract class Character extends DrawableSceneElement {

	protected boolean moveLeft = false;
	protected boolean moveRight = false;
	protected boolean moveUp = false;
	protected boolean moveDown = false;

	protected boolean isGravityEnabled = true;

	protected GameController game;
	protected int savedX;
	protected int savedY;

	/**
	 * The number of grid cells we move in one update, with normal velocity
	 */
	protected int gridCellDistance;
	protected int updatesTillAnotherStep;
	protected int updatesBetweenSteps;

	private final static int DEFAULT_UPDATES_BETWEEN_STEPS = 3;

	private final static int UPDATES_TO_DIE = 10;

	private int updatesTillStopDying;
	private boolean alive;
	protected int lives;

	public Character(Sprite.Transparency transparency) {
		super(transparency);
		updatesBetweenSteps = DEFAULT_UPDATES_BETWEEN_STEPS;
		updatesTillStopDying = UPDATES_TO_DIE;
		alive = true;
		lives = 1;
	}

	public void setGameController(GameController gameController) {
		this.game = gameController;
	}

	public void startMovingLeft() {
		moveLeft = true;
	}

	public void stopMovingLeft() {
		moveLeft = false;
	}

	public void startMovingRight() {
		moveRight = true;
	}

	public void stopMovingRight() {
		moveRight = false;
	}

	public void startMovingUp() {
		moveUp = true;
	}

	public void stopMovingUp() {
		moveUp = false;
	}

	public void startMovingDown() {
		moveDown = true;
	}

	public void stopMovingDown() {
		moveDown = false;
	}

	protected abstract void updateXPosition();
	protected abstract void updateGravity();
	protected abstract void updateYPosition();

	public void updatePosition() {
		if(!alive) {
			handleDeath();
			return;
		}
		updateXPosition();
		updateGravity();
		updateYPosition();
		updateFrame();
	}

	protected void updateFrame() {
		updatesTillAnotherStep--;
		if(updatesTillAnotherStep == 0) {
			setNextFrame();
			updatesTillAnotherStep = updatesBetweenSteps;
		}
	}

	private void resurrect() {
		alive = true;
		restoreDefaultPosition();
	}

	private void handleDeath() {
		toggleVisibility();
		if(updatesTillStopDying > 0) {
			updatesTillStopDying--;
			if(updatesTillStopDying == 0) {
				resurrect();
			}
		}
	}

	public void die() {
		if(alive) {
			updatesTillStopDying = UPDATES_TO_DIE;
			alive = false;
		}
	}
}
