package com.staticdot.apebusiness;

public class Banana extends DrawableSceneElement {

	private static final int POSITION_ONE = 0;
	private static final int POSITION_TWO = 1;

	private final static int UPDATES_BETWEEN_FRAMES = 10;

	private int frame = 1;
	private int updatesTillAnotherStep;

	public Banana(Rect bounds) {
		super(Sprite.Transparency.TRANSLUCENT);
		this.bounds = bounds;
		addResourceName(POSITION_ONE, "banana01");
		addResourceName(POSITION_TWO, "banana02");
		updatesTillAnotherStep = UPDATES_BETWEEN_FRAMES;
	}

	@Override
	protected void setNextFrame() {
		frame++;
		frame = (frame % 2);
		activeSpriteId = frame;
	}

	public void update() {
		updatesTillAnotherStep--;
		if(updatesTillAnotherStep == 0) {
			setNextFrame();
			updatesTillAnotherStep = UPDATES_BETWEEN_FRAMES;
		}
	}
}
