package com.staticdot.apebusiness;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.wwg.tools.swing.TimedKeyListener;

public class ImagePanel extends JPanel implements Runnable {

	/**
	 * The serial version UID of this class
	 */
	private static final long serialVersionUID = 1L;
	private static final int fps = 30;
	/** How long a frame should take */
	private static final long period = 1000000L * (1000 / fps);

	/**
	 * Number of frames with a delay of 0 ms before the
	 * animation thread yields to other running threads
	 */
	private static final int NO_DELAYS_PER_YIELD = 16;
	/**
	 * Number of frames that can be skipped in one animation loop
	 * i.e. the games state is updated but not rendered
	 */
	private static final int MAX_FRAME_SKIPS = 5;

	private Thread animator;
	private boolean running = false; // stops the animation

	private GameController gameController;
	private JFrame parentFrame;

	public ImagePanel(JFrame parentFrame, int maxWidth, int maxHeight) {
		this.parentFrame = parentFrame;
		gameController = new GameController(maxWidth, maxHeight);
		int actualWidth = gameController.getScene().getScreenWidth();
		int actualHeight = gameController.getScene().getScreenHeight();

		setIgnoreRepaint(true);
		setPreferredSize(new Dimension(actualWidth, actualHeight));
		setFocusable(true);
		requestFocus(); // JPanel now receives key events
		readyForTermination();
		// Listen for mouse presses
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				gameController.touch(e.getX(), e.getY());
			}
		});
	}

	private void readyForTermination() {
		/*
		addKeyListener(new KeyAdapter() {
			// listen for esc, q, end, ctrl-c
			public void keyPressed(KeyEvent e) {
				System.out.println("down: " + e.getKeyCode());
				int keyCode = e.getKeyCode();
				if(keyCode == KeyEvent.VK_ESCAPE ||
						keyCode == KeyEvent.VK_Q ||
						keyCode == KeyEvent.VK_END ||
						((keyCode == KeyEvent.VK_C) && e.isControlDown())) {
					running = false;
				}
			}
			public void keyReleased(KeyEvent e) {
				System.out.println("up: " + e.getKeyCode());
			}
		});
		 */
		addKeyListener(keyListener);
	}

	private KeyListener keyListener = new TimedKeyListener() {
		public void KeyPressed(KeyEvent e) {
			switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				gameController.startLeft();
				break;
			case KeyEvent.VK_RIGHT:
				gameController.startRight();
				break;
			case KeyEvent.VK_UP:
				gameController.startUp();
				break;
			case KeyEvent.VK_DOWN:
				gameController.startDown();
				break;
			}
		}

		public void KeyReleased(KeyEvent e) {
			switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				gameController.stopLeft();
				break;
			case KeyEvent.VK_RIGHT:
				gameController.stopRight();
				break;
			case KeyEvent.VK_UP:
				gameController.stopUp();
				break;
			case KeyEvent.VK_DOWN:
				gameController.stopDown();
				break;
			case KeyEvent.VK_1:
				setResolution(640, 480);
				break;
			case KeyEvent.VK_2:
				setResolution(800, 600);
				break;
			case KeyEvent.VK_D:
				gameController.toggleDebugMode();
				break;
			case KeyEvent.VK_M:
				gameController.toggleMuteMusic();
				break;
			case KeyEvent.VK_N:
				gameController.toggleMuteSound();
				break;
			}
		}

		public void KeyTyped(KeyEvent e) {
			// Not needed
		}
	};

	@Override
	public void addNotify() {
		// Wait for the JPanel to be added to the JFrame/JApplet before starting
		super.addNotify(); // creates the peer
		startGame(); // create the thread
	}

	private void startGame() {
		// Initialize and start the thread
		if(animator == null && !running) {
			animator = new Thread(this);
			animator.start();
		}
	}

	public void stopGame() {
		// Called by the user to stop execution
		running = false;
	}

	public void run() {
		/* Repeatedly update, render, sleep so loop takes close
		 * to period nsecs. Sleep inaccuracies are handled.
		 * Overruns in updates/renders will cause extra updates
		 * to be carried out so UPS ~== requested FPS
		 */
		long beforeTime;
		long afterTime;
		long timeDiff;
		long sleepTime;
		long overSleepTime = 0L;
		int noDelays = 0;
		long excess = 0L;
		running = true;
		while(running) {
			beforeTime = System.nanoTime();

			gameController.gameUpdate(); // game state is updated
			gameController.gameRender(); // render to a buffer
			paintScreen(); // draw buffer to screen

			afterTime = System.nanoTime();
			timeDiff = afterTime - beforeTime;
			sleepTime = (period - timeDiff) - overSleepTime; // time left in this loop;
			if(sleepTime > 0) {
				// Some time left in this cycle
				try {
					Thread.sleep(sleepTime / 1000000L);
				} catch(InterruptedException e) {}
				overSleepTime = (System.nanoTime() - afterTime) - sleepTime;
			} else {
				// sleepTime <= 0; frame took longer than the period
				excess -= sleepTime; // store excess time value
				overSleepTime = 0L;
				if(++noDelays >= NO_DELAYS_PER_YIELD) {
					Thread.yield(); // give another thread a chance to run
					noDelays = 0;
				}
			}
			/* If frame animation is taking too long, update the game state
			 * without rendering it, to get the updates/sec nearer to
			 * the required FPS.
			 */
			int skips = 0;
			while((excess > period) && (skips < MAX_FRAME_SKIPS)) {
				excess -= period;
				gameController.gameUpdate(); // update state but don't render
				skips++;
				System.out.println("skipping frame");
			}
		}
	}

	private void paintScreen() {
		// Actively render the buffer image to the screen
		Graphics g; // can change, needs to be freshly obtained every time
		Sprite screen;
		try {
			g = this.getGraphics(); // get the panel's graphic context
			screen = gameController.getScene().getScreen();
			if((g != null) && (screen != null)) {
				screen.blit(g, 0, 0);
			}
			g.dispose();
		} catch(Exception e) {
			System.out.println("Graphics context error: " + e);
		}
	}

	private void setResolution(int width, int height) {
		// Change the images
		gameController.setResolution(width, height);
		// Resize the screen
		setSize(width, height);
		setPreferredSize(new Dimension(width, height));
		parentFrame.pack();
	}
}
