package com.staticdot.apebusiness;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

	private JFrame frame;
	private static final String WINDOW_TITLE = "Ape Business";

	private void centerFrameOnScreen() {
		// Get the size of the screen
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		// Determine the new location of the window
		int w = frame.getSize().width;
		int h = frame.getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;

		// Move the window
		frame.setLocation(x, y);
	}

	private Dimension getMaxSize() {
		Dimension max = Toolkit.getDefaultToolkit().getScreenSize();
		// The actual screen height should not match, because of extra
		// window decorations. For this reason, we make the height slightly smaller
		// in order to force the largest resolution _smaller_ than the screen size
		max.height -= 10;
		return max;
	}

	public void setup() {
		// Create and set up the window.
		frame = new JFrame(WINDOW_TITLE);
		frame.setLocationRelativeTo(null); // Center on screen
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add the play zone panel.
		Dimension maxSize = getMaxSize();
		JPanel playZone = new ImagePanel(frame, maxSize.width, maxSize.height);
		frame.getContentPane().add(playZone);
	}

	public void display() {
		// Display the window.
		frame.pack();
		centerFrameOnScreen();
		frame.setVisible(true);		
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		Main window = new Main();
		window.setup();
		window.display();
	}

	public static void main(String[] args) {
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
