package com.staticdot.apebusiness;

public class Background extends DrawableSceneElement {

	public Background() {
		super(Sprite.Transparency.OPAQUE);
		bounds = new Rect(0, 0, 160, 120);
	}

	public void setResourceName(String name) {
		addResourceName(0, name);
	}

	@Override
	protected void setNextFrame() {
		// There's only one frame
	}
}
