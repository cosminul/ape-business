package com.staticdot.apebusiness;

public class Rect {

	public int x;
	public int y;
	public int w;
	public int h;

	private int left;
	private int right;
	private int top;
	private int bottom;

	public enum CollisionType {
		NO_COLLSION,
		TOP_COLLISION,
		TOP_TOUCH,
		BOTTOM_COLLISION,
		BOTTOM_TOUCH,
		LEFT_COLLISION,
		LEFT_TOUCH,
		RIGHT_COLLISION,
		RIGHT_TOUCH
	};

	public Rect(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		calculateSides();
	}

	private boolean between(int a, int x, int b) {
		if((a <= x) && (x <= b)) {
			return true;
		}
		return false;
	}

	private void calculateSides() {
		// Calculate the sides of this rectangle
		left = x;
		right = x + w - 1;
		top = y;
		bottom = y + h - 1;
	}

	private CollisionType getTouchType(Rect other) {
		if(other.top - 1 == bottom) {
			return CollisionType.BOTTOM_TOUCH;
		}
		if(other.bottom + 1 == top) {
			return CollisionType.TOP_TOUCH;
		}
		if(other.right + 1 == left) {
			return CollisionType.LEFT_TOUCH;
		}
		if(other.left - 1 == right) {
			return CollisionType.RIGHT_TOUCH;
		}
		return CollisionType.NO_COLLSION;
	}

	public CollisionType getCollisionType(Rect other) {
		calculateSides();
		other.calculateSides();
		CollisionType touch = getTouchType(other);
		if(touch != CollisionType.NO_COLLSION) {
			return touch;
		}
		// Bottom collision is the most frequent (because of gravity)
		// so we check this one first
		if(between(other.top, bottom, other.bottom)) {
			return CollisionType.BOTTOM_COLLISION;
		}
		if(between(other.top, top, other.bottom)) {
			return CollisionType.TOP_COLLISION;
		}
		if(between(other.left, left, other.right)) {
			return CollisionType.LEFT_COLLISION;
		}
		if(between(other.left, right, other.right)) {
			return CollisionType.RIGHT_COLLISION;
		}
		return CollisionType.NO_COLLSION;
	}

	public boolean touches(Rect other) {
		return collides(other, 1);
	}

	public boolean collides(Rect other) {
		return collides(other, 0);
	}

	private boolean collides(Rect other, int minimumDistance) {		
		calculateSides();
		other.calculateSides();
		// If any of the sides from this are outside of other
		if(bottom + minimumDistance < other.top) {
			return false;
		}
		if(top - minimumDistance > other.bottom) {
			return false;
		}
		if(right + minimumDistance < other.left) {
			return false;
		}
		if(left - minimumDistance > other.right) {
			return false;
		}
		// If none of the sides from this are outside other
		return true;
	}
}
