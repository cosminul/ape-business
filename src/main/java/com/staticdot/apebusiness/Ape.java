package com.staticdot.apebusiness;

public class Ape extends Character {

	/**
	 * This is decreased every time we update. When it becomes 0, jumping stops
	 * and the ape begins to fall. After the ape reaches the ground the energy
	 * is refilled.
	 */
	private int jumpEnergy;
	private final static int DEFAULT_JUMP_ENERGY = 10;

	// Stay
	private static final int POSITION_STAY = 0;
	// Walk right
	private static final int POSITION_WALK_RIGHT_1 = 1;
	private static final int POSITION_WALK_RIGHT_2 = 2;
	private static final int POSITION_WALK_RIGHT_3 = 3;
	// Walk left
	private static final int POSITION_WALK_LEFT_1 = 4;
	private static final int POSITION_WALK_LEFT_2 = 5;
	private static final int POSITION_WALK_LEFT_3 = 6;
	// Climb stairs
	private static final int POSITION_CLIMB_1 = 7;
	private static final int POSITION_CLIMB_2 = 8;
	private static final int POSITION_CLIMB_3 = 9;

	private SpriteAnimator animator;
	private SpriteAnimator.SpriteSequence staySequence;
	private SpriteAnimator.SpriteSequence walkLeftSequence;
	private SpriteAnimator.SpriteSequence walkRightSequence;
	private SpriteAnimator.SpriteSequence climbSequence;

	private final static int UPDATES_BETWEEN_STEPS = 3;

	public Ape() {
		super(Sprite.Transparency.TRANSLUCENT);
		bounds = new Rect(5, 35, 18, 14);
		jumpEnergy = DEFAULT_JUMP_ENERGY;
		gridCellDistance = 1;
		updatesBetweenSteps = UPDATES_BETWEEN_STEPS;
		updatesTillAnotherStep = updatesBetweenSteps;
		lives = 3;
		setupSprites();
		setupAnimation();
	}

	private void setupSprites() {
		// Stay
		addResourceName(POSITION_STAY, "monkey01");
		// Walk left
		addResourceName(POSITION_WALK_RIGHT_1, "monkey02");
		addResourceName(POSITION_WALK_RIGHT_2, "monkey03");
		addResourceName(POSITION_WALK_RIGHT_3, "monkey04");
		// Walk right - the same sprites as for walk left, but flipped horizontally
		addResourceName(POSITION_WALK_LEFT_1, "monkey02", true);
		addResourceName(POSITION_WALK_LEFT_2, "monkey03", true);
		addResourceName(POSITION_WALK_LEFT_3, "monkey04", true);
		// Climb stairs
		addResourceName(POSITION_CLIMB_1, "monkey05");
		addResourceName(POSITION_CLIMB_2, "monkey06");
		addResourceName(POSITION_CLIMB_3, "monkey05", true);
	}

	private void setupAnimation() {
		animator = new SpriteAnimator();
		// Stay
		staySequence = new SpriteAnimator.SpriteSequence();
		staySequence.addSpriteId(POSITION_STAY);
		animator.addSequence(staySequence);
		// Walk left
		walkLeftSequence = new SpriteAnimator.SpriteSequence();
		walkLeftSequence.addSpriteId(POSITION_WALK_LEFT_1);
		walkLeftSequence.addSpriteId(POSITION_WALK_LEFT_2);
		walkLeftSequence.addSpriteId(POSITION_WALK_LEFT_3);
		walkLeftSequence.addSpriteId(POSITION_WALK_LEFT_2);
		animator.addSequence(walkLeftSequence);
		// Walk right
		walkRightSequence = new SpriteAnimator.SpriteSequence();
		walkRightSequence.addSpriteId(POSITION_WALK_RIGHT_1);
		walkRightSequence.addSpriteId(POSITION_WALK_RIGHT_2);
		walkRightSequence.addSpriteId(POSITION_WALK_RIGHT_3);
		walkRightSequence.addSpriteId(POSITION_WALK_RIGHT_2);
		animator.addSequence(walkRightSequence);
		// Climb stairs
		climbSequence = new SpriteAnimator.SpriteSequence();
		climbSequence.addSpriteId(POSITION_CLIMB_1);
		climbSequence.addSpriteId(POSITION_CLIMB_2);
		climbSequence.addSpriteId(POSITION_CLIMB_3);
		climbSequence.addSpriteId(POSITION_CLIMB_2);
		animator.addSequence(climbSequence);
	}

	@Override
	protected void setNextFrame() {
		Rect.CollisionType stairCollision = game.getScene().stairsHitByBox(bounds);
		Rect.CollisionType obstacleCollision = game.getScene().obstaclesTouchedByBox(bounds);
		boolean hitStairs = (stairCollision != Rect.CollisionType.NO_COLLSION);
		boolean hitGround = (obstacleCollision == Rect.CollisionType.BOTTOM_TOUCH);

		/* Change to a climbing image when over a stair
		 * (but not when crossing it while walking)
		 */
		if(hitStairs && !hitGround) {
			animator.setActiveSequence(climbSequence);
			/* Change to a climbing image not only when moving up or down,
			 * but also when moving left or right, since it is possible to
			 * cross a stair while jumping horizontally (and then we want to
			 * hang on to the stair).
			 */
			if(moveUp || moveDown || moveLeft || moveRight) {
				activeSpriteId = animator.nextSpriteId();
			}
			return;
		}

		if(moveRight) {
			animator.setActiveSequence(walkRightSequence);
		} else if(moveLeft) {
			animator.setActiveSequence(walkLeftSequence);
		} else {
			animator.setActiveSequence(staySequence);
		}
		activeSpriteId = animator.nextSpriteId();
	}

	@Override
	protected void updateGravity() {
		if(game.getScene().stairsHitByBox(bounds) != Rect.CollisionType.NO_COLLSION) {
			isGravityEnabled = false;
		} else {
			isGravityEnabled = true;			
		}
	}

	@Override
	protected void updateXPosition() {
		savedX = bounds.x;

		// Move left / right as requested
		if(moveLeft) {
			bounds.x -= gridCellDistance;
		} else if(moveRight) {
			bounds.x += gridCellDistance;
		}

		if(game.getScene().obstaclesHitByBox(bounds) != Rect.CollisionType.NO_COLLSION) {
			// This move would collide; restore position
			bounds.x = savedX;
		}
	}

	private void verticalMoveAsRequested() {
		// Move up / down as requested
		if(moveUp) {
			if(game.getScene().stairsHitByBox(bounds) != Rect.CollisionType.NO_COLLSION) {
				bounds.y -= gridCellDistance;
			} else if(jumpEnergy > 0) {
				isGravityEnabled = false;
				bounds.y -= 2 * gridCellDistance;
				jumpEnergy--;
			}
		} else if(moveDown) {
			bounds.y += gridCellDistance;
		}
	}

	private void fall() {
		jumpEnergy = 0; // Do not allow to start jumping while falling
		bounds.y += gridCellDistance;
	}

	private void restoreYPositionIfNeeded(boolean checkStairs) {
		Rect.CollisionType obstacleCollision = game.getScene().obstaclesHitByBox(bounds);
		Rect.CollisionType stairCollision = game.getScene().stairsHitByBox(bounds);
		boolean hitObstacles = (obstacleCollision != Rect.CollisionType.NO_COLLSION);
		boolean hitStairs = false;
		if(checkStairs) {
			hitStairs = (stairCollision != Rect.CollisionType.NO_COLLSION);
		}
		if(hitObstacles || hitStairs) {
			// Restore jump energy if it hit the ground.
			// This is to avoid hanging on roofs
			if(obstacleCollision == Rect.CollisionType.BOTTOM_COLLISION) {
				jumpEnergy = DEFAULT_JUMP_ENERGY;
			}
			if(stairCollision == Rect.CollisionType.BOTTOM_COLLISION) {
				jumpEnergy = DEFAULT_JUMP_ENERGY;
			}
			// This move would collide; restore position
			bounds.y = savedY;
		}
	}

	@Override
	protected void updateYPosition() {
		savedY = bounds.y;
		verticalMoveAsRequested();
		restoreYPositionIfNeeded(false);
		// Fall
		if(isGravityEnabled) {
			savedY = bounds.y;
			fall();
			restoreYPositionIfNeeded(true);
		}
	}
}
